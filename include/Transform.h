#ifndef _TRANSFORM_H
#define _TRANSFORM_H

#include "Matrix.h"
#include "Point.h"
#include "Vector.h"
#include "Normal.h"
#include "Ray.h"

namespace pdrt
{
  class Transform
  {
  public:
    Transform();
    explicit Transform( const Matrix& m );
    Transform( const Transform& rhs );
    Transform( Transform&& rhs );
    ~Transform();

    Transform& operator= ( const Transform& rhs );
    Transform& operator= ( Transform&& rhs );

    Point operator() ( const Point& p );
    Vector operator() ( const Vector& v );
    Normal operator() ( const Normal& n );
    Ray operator() ( const Ray& r );

  private:
    Matrix _m;
    Matrix _mInv;
  };
}

#endif // _TRANSFORM_H