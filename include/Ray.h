#ifndef _RAY_H
#define _RAY_H

#include "Vector.h"
#include "Point.h"

namespace pdrt
{
  class Ray
  {
  public:
    Ray();
    Ray( const Point& o, const Vector& d );
    Ray( const Ray& rhs );
    Ray( Ray&& rhs );
    ~Ray();

    Ray& operator= ( const Ray& rhs );
    Ray& operator= ( Ray&& rhs );

  private:
    Point _origin;
    Vector _dir;
  };
}

#endif // _RAY_H