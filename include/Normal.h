#ifndef _NORMAL_H
#define _NORMAL_H

namespace pdrt
{
  class Normal
  {
  public:
    Normal();
    Normal( const Normal& rhs );
    Normal( Normal&& rhs );
    ~Normal();

    Normal& operator= ( const Normal& rhs );
    Normal& operator= ( Normal&& rhs );

    double& operator[] ( int index );
    const double& operator[] ( int index ) const;

    double Length() const;
    double LengthSquared() const;
    Normal& Normalize();

  private:
    union
    {
      double _n[ 4 ];
      double _x, _y, _z, _w;
    };
  };
}

#endif // _NORMAL_H