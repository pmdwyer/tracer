#ifndef _BBOX_H
#define _BBOX_H

#include "Point.h"

namespace pdrt
{
  class BBox
  {
  public:
    BBox();
    BBox( const Point& lowerLeft, const Point& upperRight );
    BBox( const Point& lowerLeft, double width, double height, double depth );
    BBox( const BBox& rhs );
    BBox( BBox&& rhs );
    ~BBox();

    BBox& operator= ( const BBox& rhs );
    BBox& operator= ( BBox&& rhs );

    // TODO: make contains point, intersect ray, overlaps other bbox methods

  private:
    Point _min;
    Point _max;
  };
}

#endif // _BBOX_H