#ifndef _AXIS_H
#define _AXIS_H

namespace pdrt
{
  enum class Axis
  {
    X,
    Y,
    Z,
    End
  };
}

#endif // _AXIS_H