#ifndef _SCENE_H
#define _SCENE_H

#include "Camera.h"
#include "World.h"

namespace pdrt
{
  class Scene
  {
  public:
    Scene();
    // TODO: add methods to get/set camera and world
    Scene( const Scene& rhs );
    Scene( Scene&& rhs );
    ~Scene();

    Scene& operator= ( const Scene& rhs );
    Scene& operator= ( Scene&& rhs );

  private:
    Camera _camera;
    World _world;
  };
}

#endif // _SCENE_H