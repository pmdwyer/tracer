#ifndef _MATRIXSTACK_H
#define _MATRIXSTACK_H

#include <vector>

#include "Matrix.h"

namespace pdrt
{
  class MatrixStack
  {
  public:
    MatrixStack();
    ~MatrixStack();

    void Push( const Matrix& m );
    void Pop();
    const Matrix Top();

  private:
    std::vector< Matrix > _stack;
  };
}

#endif // _MATRIXSTACK_H