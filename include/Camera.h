#ifndef _CAMERA_H
#define _CAMERA_H

namespace pdrt
{
  class Camera
  {
  public:
    Camera();
    Camera( const Camera& rhs );
    Camera( Camera&& rhs );
    ~Camera();

    Camera& operator= ( const Camera& rhs );
    Camera& operator= ( Camera&& rhs );
  
  private:
    // TODO: add film and transform members
  };
}

#endif //_CAMERA_H