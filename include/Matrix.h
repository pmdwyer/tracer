#ifndef _MATRIX_H
#define _MATRIX_H

#include "Axis.h"
#include "Vector.h"
#include "Point.h"
#include "Normal.h"
#include "Ray.h"

namespace pdrt
{
  class Matrix
  {
  public:
    Matrix();
    Matrix( const Vector& x, const Vector& y, const Vector& z, const Point& o );
    Matrix( const Matrix& rhs );
    Matrix( Matrix&& rhs );
    ~Matrix();

    Matrix& operator= ( const Matrix& rhs );
    Matrix& operator= ( Matrix&& rhs );

    double* operator[] ( int row );
    const double* operator[] ( int row ) const;

    Matrix& operator*= ( const Matrix& m );

    Matrix& Inverse();
    Matrix& Transpose();

    static Matrix Translate( double x, double y, double z );
    static Matrix Scale( double x, double y, double z );
    static Matrix Rotate( const Axis& axis, double radians );

  private:
    double _mat[ 4 ][ 4 ];

    static Matrix RotateX( double radians );
    static Matrix RotateY( double radians );
    static Matrix RotateZ( double radians );
  };

  Matrix operator* ( const Matrix& left, const Matrix& right );
  Point operator* ( const Matrix& left, const Point& right );
  Vector operator* ( const Matrix& left, const Vector& right );
  Normal operator* ( const Matrix& left, const Normal& right );
  Ray operator* ( const Matrix& left, const Ray& right );

  Matrix Inverse( const Matrix& m );
  Matrix Transpose( const Matrix& m );
}

#endif // _MATRIX_H