#ifndef _VECTOR_H
#define _VECTOR_H

namespace pdrt
{
  class Point;

  class Vector
  {
  public:
    Vector( double x = 0.0, double y = 0.0, double z = 0.0 );
    explicit Vector( const Point& p );
    Vector( const Vector& rhs );
    Vector( Vector&& rhs );
    ~Vector();

    Vector& operator= ( const Vector& rhs );
    Vector& operator= ( Vector&& rhs );

    double& operator[] ( int index );
    const double& operator[] ( int index ) const;

    Vector& operator+= ( const Vector& v );
    Vector& operator-= ( const Vector& v );
    Vector& operator*= ( const int scale );

    double Dot( const Vector& v ) const;
    Vector Cross( const Vector& v ) const;

    Vector& Normalize();
    double Length() const;
    double LengthSquared() const;

  private:
    union
    {
      double _v[ 4 ];
      double _x, _y, _z, _w;
    };
  };

  Vector operator+ ( const Vector& left, const Vector& right );
  Vector operator- ( const Vector& left, const Vector& right );
  Vector operator* ( const Vector& v, const int s );
  Vector operator* ( const int s, const Vector& v );
}

#endif // _VECTOR_H