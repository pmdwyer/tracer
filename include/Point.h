#ifndef _POINT_H
#define _POINT_H

#include "Vector.h"

namespace pdrt
{
  class Point
  {
  public:
    Point();
    Point( double x, double y, double z );
    Point( const Point& rhs );
    Point( Point&& rhs );
    ~Point();

    Point& operator= ( const Point& rhs );
    Point& operator= ( Point&& rhs );

    Point& operator-= ( const Vector& v );
    Point& operator+= ( const Vector& v );

    Vector Subtract( const Point& p ) const;

    double& operator[] ( int index );
    const double& operator[] ( int index ) const;

  private:
    union
    {
      double _p[ 4 ];
      double _x, _y, _z, _w;
    };
  };

  Vector operator- ( const Point& p1, const Point& p2 );
  Point operator+ ( const Point& p, const Vector& v );
  Point operator+ ( const Vector& v, const Point& p );
}

#endif // _POINT_H