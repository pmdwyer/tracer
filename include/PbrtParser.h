#ifndef _PARSER_H
#define _PARSER_H

#include <string>
#include <fstream>
#include <vector>

#include "Scene.h"

namespace pdrt
{
  class PbrtParser
  {
  public:
    static Scene Parse( std::istream& fin );

  private:
    PbrtParser();
    ~PbrtParser();

    typedef struct parameter
    {
      std::string type;
      std::string name;
      std::vector< std::string > vals;
    } Parameter;

    static std::string ParseType( std::istream& fin );
    static std::vector< Parameter > ParseParameters( std::istream& fin );
    static void ConsumeSpaces( std::istream& fin );
  };
}

#endif // _PARSER_H