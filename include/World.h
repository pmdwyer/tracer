#ifndef _WORLD_H
#define _WORLD_H

namespace pdrt
{
  class World
  {
  public:
    World();
    World( const World& rhs );
    World( World&& rhs );
    ~World();

    World& operator= ( const World& rhs );
    World& operator= ( World&& rhs );
    
  private:
  };
}

#endif // _WORLD_H