#include "Transform.h"

#include <utility>

using namespace pdrt;

Transform::Transform()
  : _m{},
    _mInv{}
{
}

Transform::Transform( const Matrix& m )
  : _m{ m },
    _mInv{ Inverse( m ) }
{
}

Transform::Transform( const Transform& rhs )
  : _m{ rhs._m },
    _mInv{ rhs._mInv }
{
}

Transform::Transform( Transform&& rhs )
  : _m{ std::move( rhs._m ) },
    _mInv{ std::move( rhs._mInv ) }
{
}

Transform::~Transform()
{
}

Transform& Transform::operator= ( const Transform& rhs )
{
  _m = rhs._m;
  _mInv = rhs._mInv;
  return *this;
}

Transform& Transform::operator= ( Transform&& rhs )
{
  _m = std::move( rhs._m );
  _mInv = std::move( rhs._mInv );
  return *this;
}

Point Transform::operator() ( const Point& p )
{
  return _m * p;
}

Vector Transform::operator() ( const Vector& v )
{
  return _m * v;
}

Normal Transform::operator() ( const Normal& n )
{
  // TODO: check this formula
  return _mInv * n;
}

Ray Transform::operator() ( const Ray& r )
{
  return _m * r;
}