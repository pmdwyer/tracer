#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

#include "PbrtParser.h"
#include "Scene.h"

using namespace std;
using namespace pdrt;

void PrintUsage();

int main( int argc, char* argv[] )
{
  if ( argc < 2 )
  {
    PrintUsage();
    return 0;
  }

  // TODO: check that input file actually exists, has correct perms, etc.
  ifstream fin( argv[ 1 ] );

  Scene scene( PbrtParser::Parse( fin ) );

  char c;
  cin >> c;

  return 0;
}

void PrintUsage()
{
  cout << "tracer <input_file>.pbrt" << endl;
}