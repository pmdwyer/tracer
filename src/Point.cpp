#include "Point.h"

using namespace pdrt;

Point::Point()
  : _x{ 0.0 }, _y{ 0.0 }, _z{ 0.0 }, _w{ 1.0 }
{
}

Point::Point( double x, double y, double z )
  : _x{ x }, _y{ y }, _z{ z }, _w{ 1.0 }
{
}

Point::Point( const Point& rhs )
  : _x{ rhs._x }, _y{ rhs._y }, _z{ rhs._z }, _w{ rhs._w }
{
}

Point::Point( Point&& rhs )
  : _x{ rhs._x }, _y{ rhs._y }, _z{ rhs._z }, _w{ rhs._w }
{
}

Point::~Point()
{
}

Point& Point::operator= ( const Point& rhs )
{
  _x = rhs._x;
  _y = rhs._y;
  _z = rhs._z;
  _w = rhs._w;
  return *this;
}

Point& Point::operator= ( Point&& rhs )
{
  _x = rhs._x;
  _y = rhs._y;
  _z = rhs._z;
  _w = rhs._w;
  return *this;
}

Point& Point::operator-= ( const Vector& v )
{
  _x -= v[ 0 ];
  _y -= v[ 1 ];
  _z -= v[ 2 ];
  return *this;
}

Point& Point::operator+= ( const Vector& v )
{
  _x += v[ 0 ];
  _y += v[ 1 ];
  _z += v[ 2 ];
  return *this;
}

Vector Point::Subtract( const Point& p ) const
{
  Vector v{ _x - p._x, _y - p._y, _z - p._z };
  return v;
}

double& Point::operator[] ( int index )
{
  return _p[ index ];
}

const double& Point::operator[] ( int index ) const
{
  return _p[ index ];
}

Vector pdrt::operator- ( const Point& p1, const Point& p2 )
{
  Vector v = p1.Subtract( p2 );
  return v;
}

Point pdrt::operator+ ( const Point& p, const Vector& v )
{
  Point ret = p;
  ret += v;
  return ret;
}

Point pdrt::operator+ ( const Vector& v, const Point& p )
{
  Point ret = p;
  ret += v;
  return ret;
}