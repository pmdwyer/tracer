#include "Scene.h"

#include <utility>

using namespace pdrt;

Scene::Scene()
  : _camera{},
    _world{}
{

}

Scene::Scene( const Scene& rhs )
  : _camera{ rhs._camera },
    _world{ rhs._world }
{
}

Scene::Scene( Scene&& rhs )
  : _camera{ std::move( rhs._camera ) },
    _world{ std::move( rhs._world ) }
{
}

Scene::~Scene()
{
}

Scene& Scene::operator= ( const Scene& rhs )
{
  _camera = rhs._camera;
  _world = rhs._world;
  return *this;
}

Scene& Scene::operator= ( Scene&& rhs )
{
  _camera = std::move( rhs._camera );
  _world = std::move( rhs._world );
  return *this;
}