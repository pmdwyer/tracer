#include "BBox.h"

#include <algorithm>

using namespace pdrt;

// TODO: make degenerate points by default
BBox::BBox()
  : _min(),
    _max()
{
}

BBox::BBox( const Point& lowerLeft, const Point& upperRight )
  : _min{ lowerLeft },
    _max{ upperRight }
{
}

BBox::BBox( const Point& lowerLeft, double width, double height, double depth )
{
  _min = lowerLeft;
  _max = lowerLeft;
  _max[ 0 ] += width;
  _max[ 1 ] += height;
  _max[ 2 ] += depth;
}

BBox::BBox( const BBox& rhs )
  : _min{ rhs._min },
    _max{ rhs._max }
{
}

BBox::BBox( BBox&& rhs )
  : _min{ std::move( rhs._min ) },
    _max{ std::move( rhs._max ) }
{
}

BBox::~BBox()
{
}

BBox& BBox::operator= ( const BBox& rhs )
{
  _min = rhs._min;
  _max = rhs._max;
  return *this;
}

BBox& BBox::operator= ( BBox&& rhs )
{
  _min = std::move( rhs._min );
  _max = std::move( rhs._max );
  return *this;
}