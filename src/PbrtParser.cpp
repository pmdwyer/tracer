#include "PbrtParser.h"

#include <iostream>
#include <string>
#include <sstream>
#include <cctype>

#include "MatrixStack.h"

using namespace std;

pdrt::Scene pdrt::PbrtParser::Parse( istream& fin )
{
  Scene scene;
  MatrixStack matrixStack;
  string line, word, type;
  vector< Parameter > currentParams;

  // need to preprocess input for Include directive

  while ( !fin.fail() )
  {
    stringstream lineParser;
    getline( fin, line );

    // trim whitespace and check for empty lines
    line.erase( line.find_last_not_of( " \r\n\v\t\f" ) + 1 );
    line.erase( 0, line.find_first_not_of( " \r\n\v\t\f" ) );
    if ( line.empty() )
    {
      continue;
    }

    // check for comment lines
    if ( line[ 0 ] == '#' )
    {
      continue;
    }

    lineParser << line;
    lineParser >> word;
    if ( word == "Include" )
    {
      // recursive parse file
      continue;
    }

    // Scene wide rendering options
    if ( word == "Camera" )
    {
      type = ParseType( lineParser );
      currentParams = ParseParameters( lineParser );
    }
    else if ( word == "Film" )
    {
      type = ParseType( lineParser );
      currentParams = ParseParameters( lineParser );
    }
    else if ( word == "Filter" )
    {
      type = ParseType( lineParser );
      currentParams = ParseParameters( lineParser );
    }
    else if ( word == "Renderer" )
    {
      type = ParseType( lineParser );
      currentParams = ParseParameters( lineParser );
    }
    else if ( word == "Sampler" )
    {
      type = ParseType( lineParser );
    }
    else if ( word == "SurfaceIntegrator" )
    {
      type = ParseType( lineParser );
      currentParams = ParseParameters( lineParser );
    }
    else if ( word == "Volume" )
    {
      type = ParseType( lineParser );
      currentParams = ParseParameters( lineParser );
    }
    else if ( word == "Accelerator" )
    {
      type = ParseType( lineParser );
      currentParams = ParseParameters( lineParser );
    }
    else if ( word == "PixelFilter" )
    {
      type = ParseType( lineParser );
      currentParams = ParseParameters( lineParser );
    }

    // world and world objects
    else if ( word == "WorldBegin" )
    {
    }
    else if ( word == "WorldEnd" )
    {}
    else if ( word == "AttributeBegin" )
    {}
    else if ( word == "AttributeEnd" )
    {}
    else if ( word == "Shape" )
    {}
    else if ( word == "ObjectBegin" )
    {}
    else if ( word == "ObjectEnd" )
    {}
    else if ( word == "LightSource" )
    {
      type = ParseType( lineParser );
      currentParams = ParseParameters( lineParser );
    }
    else if ( word == "AreaLightSource" )
    {}
    else if ( word == "Material" )
    {}
    else if ( word == "Texture" )
    {}
    else if ( word == "ScatteringVolume" )
    {}

    // transforms
    else if ( word == "Identity" )
    {}
    else if ( word == "Translate" )
    {}
    else if ( word == "Scale" )
    {}
    else if ( word == "Rotate" )
    {}
    else if ( word == "LookAt" )
    {}
    else if ( word == "CoordinateSystem" )
    {}
    else if ( word == "CoordSysTransform" )
    {
      type = ParseType( lineParser );
    }
    else if ( word == "Transform" )
    {}
    else if ( word == "ConcatTransform" )
    {}

    // clear for next iteration
    line.clear();
    word.clear();
  }

  return scene;
}

string pdrt::PbrtParser::ParseType( std::istream& fin )
{
  string s;
  fin >> s;
  return s;
}

std::vector< pdrt::PbrtParser::Parameter > pdrt::PbrtParser::ParseParameters( std::istream& fin )
{
  std::vector< Parameter > params;
  char quote, leftBracket, rightBracket;

  // find the first non space character
  ConsumeSpaces( fin );
  quote = fin.peek();

  // consume the next paramter list
  while ( quote == '"' )
  {
    quote = fin.get();

    Parameter p;
    fin >> p.type >> p.name;

    // remove trailing quote character
    p.name.erase( p.name.length() - 1, 1 );

    // consume space and left bracket
    ConsumeSpaces( fin );
    leftBracket = fin.get();

    // parse each value as a string
    ConsumeSpaces( fin );
    while ( fin.peek() != ']' )
    {
      string value;
      char c = fin.peek();
      while ( !std::isspace( c ) &&  c != ']' )
      {
        value += fin.get();
        c = fin.peek();
      }
      p.vals.push_back( value );
      ConsumeSpaces( fin );
    }

    // consume right bracket
    rightBracket = fin.get();

    // add the newest parameter
    params.push_back( p );

    // find the next character after the parameter list
    ConsumeSpaces( fin ); 
    quote = fin.peek();
  }

  return params;
}

void pdrt::PbrtParser::ConsumeSpaces( std::istream& fin )
{
  while ( std::isspace( fin.peek() ) )
  {
    fin.get();
  }
}