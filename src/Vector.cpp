#include "Vector.h"
#include "Point.h"

#include <cmath>

using namespace pdrt;

Vector::Vector( double x, double y, double z )
  : _x{ x }, _y{ y }, _z{ z }, _w{ 0.0 }
{
}

Vector::Vector( const Point& p )
  : _x{ p[ 0 ] }, _y{ p[ 1 ] }, _z{ p[ 2 ] }, _w{ p[ 3 ] }
{
}

Vector::Vector( const Vector& rhs )
  : _x{ rhs._x }, _y{ rhs._y }, _z{ rhs._z }, _w{ rhs._w }
{
}

Vector::Vector( Vector&& rhs )
  : _x{ rhs._x }, _y{ rhs._y }, _z{ rhs._z }, _w{ rhs._w }
{
}

Vector::~Vector()
{
}

Vector& Vector::operator= ( const pdrt::Vector& rhs )
{
  _x = rhs._x;
  _y = rhs._y;
  _z = rhs._z;
  _w = rhs._w;
  return *this;
}

Vector& Vector::operator= ( pdrt::Vector&& rhs )
{
  _x = rhs._x;
  _y = rhs._y;
  _z = rhs._z;
  _w = rhs._w;
  return *this;
}

double& Vector::operator[] ( int index )
{
  return _v[ index ];
}

const double& Vector::operator[] ( int index ) const
{
  return _v[ index ];
}

Vector& Vector::operator+= ( const Vector& v )
{
  _x += v._x;
  _y += v._y;
  _z += v._z;
  return *this;
}

Vector& Vector::operator-= ( const Vector& v )
{
  _x -= v._x;
  _y -= v._y;
  _z -= v._z;
  return *this;
}

Vector& Vector::operator*= ( const int s )
{
  _x += s;
  _y += s;
  _z += s;
  return *this;
}

double Vector::Dot( const Vector& v ) const
{
  return ( ( _x * v._x ) + ( _y * v._y ) + ( _z * v._z ) );
}

Vector Vector::Cross( const Vector& v ) const
{
  Vector ret;
  ret[ 0 ] = ( _y * v._z ) - ( _z * v._y );
  ret[ 1 ] = ( _z * v._x ) - ( _x * v._z );
  ret[ 2 ] = ( _x * v._y ) - ( _y * v._x );
  return ret;
}

Vector& Vector::Normalize()
{
  double len = Length();
  _x = _x / len;
  _y = _y / len;
  _z = _z / len;
  _w = 0.0;
  return *this;
}

double Vector::Length() const
{
  return sqrt( LengthSquared() );
}

double Vector::LengthSquared() const
{
  return ( _x * _x ) + ( _y * _y ) + ( _z * _z );
}

Vector pdrt::operator+ ( const Vector& left, const Vector& right )
{
  Vector ret = left;
  ret += right;
  return ret;
}

Vector pdrt::operator- ( const Vector& left, const Vector& right )
{
  Vector ret = left;
  ret -= right;
  return ret;
}

Vector pdrt::operator* ( const Vector& v, const int s )
{
  Vector ret = v;
  ret *= s;
  return ret;
}

Vector pdrt::operator* ( const int s, const Vector& v )
{
  Vector ret = v;
  ret *= s;
  return ret;
}