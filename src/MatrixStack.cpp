#include "MatrixStack.h"

using namespace pdrt;

MatrixStack::MatrixStack()
  : _stack{}
{
}

MatrixStack::~MatrixStack()
{
}

void MatrixStack::Push( const Matrix& m )
{
  _stack.push_back( m );
}

void MatrixStack::Pop()
{
  _stack.pop_back();
}

const Matrix MatrixStack::Top()
{
  Matrix ret;
  for ( auto matrix : _stack )
  {
    ret *= matrix;   
  }
  return ret;
}