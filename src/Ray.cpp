#include "Ray.h"

#include <utility>

using namespace pdrt;

Ray::Ray()
  : _origin{},
    _dir{}
{
}

Ray::Ray( const Point& o, const Vector& d )
  : _origin{ o },
    _dir{ d }
{
}

Ray::Ray( const Ray& rhs )
  : _origin{ rhs._origin },
    _dir{ rhs._dir }
{

}

Ray::Ray( Ray&& rhs )
  : _origin{ std::move( rhs._origin ) },
    _dir{ std::move( rhs._dir ) }
{
}

Ray::~Ray()
{
}

Ray& Ray::operator= ( const Ray& rhs )
{
  _origin = rhs._origin;
  _dir = rhs._dir;
  return *this;
}

Ray& Ray::operator= ( Ray&& rhs )
{
  _origin = std::move( rhs._origin );
  _dir = std::move( rhs._dir );
  return *this;
}