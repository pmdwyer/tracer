#include "Normal.h"

#include <cmath>

using namespace pdrt;

Normal::Normal()
  : _x{ 0.0 }, _y{ 0.0 }, _z{ 0.0 }, _w{ 0.0 }
{
}

Normal::Normal( const Normal& rhs )
  : _x{ rhs._x }, _y{ rhs._y }, _z{ rhs._z }, _w{ rhs._w }
{
}

Normal::Normal( Normal&& rhs )
  : _x{ rhs._x }, _y{ rhs._y }, _z{ rhs._z }, _w{ rhs._w }
{
}

Normal::~Normal()
{
}

Normal& Normal::operator= ( const Normal& rhs )
{
  _x = rhs._x;
  _y = rhs._y;
  _z = rhs._z;
  _w = rhs._w;
  return *this;
}

Normal& Normal::operator= ( Normal&& rhs )
{
  _x = rhs._x;
  _y = rhs._y;
  _z = rhs._z;
  _w = rhs._w;
  return *this;
}

double& Normal::operator[] ( int index )
{
  return _n[ index ];
}

const double& Normal::operator[] ( int index ) const
{
  return _n[ index ];
}

double Normal::Length() const
{
  return sqrt( LengthSquared() );
}

double Normal::LengthSquared() const
{
  return ( ( _x * _x ) + ( _y * _y ) + ( _z * _z ) );
}

Normal& Normal::Normalize()
{
  double len = Length();
  _x /= len;
  _y /= len;
  _z /= len;
  _w = 0.0;
  return *this;
}