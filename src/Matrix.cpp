#include "Matrix.h"

#include <utility>
#include <cmath>

using namespace pdrt;

Matrix::Matrix()
{
  _mat[ 0 ][ 0 ] = 1.0;
  _mat[ 1 ][ 0 ] = 0.0;
  _mat[ 2 ][ 0 ] = 0.0;
  _mat[ 3 ][ 0 ] = 0.0;

  _mat[ 0 ][ 1 ] = 0.0;
  _mat[ 1 ][ 1 ] = 1.0;
  _mat[ 2 ][ 1 ] = 0.0;
  _mat[ 3 ][ 1 ] = 0.0;

  _mat[ 0 ][ 2 ] = 0.0;
  _mat[ 1 ][ 2 ] = 0.0;
  _mat[ 2 ][ 2 ] = 1.0;
  _mat[ 3 ][ 2 ] = 0.0;

  _mat[ 0 ][ 3 ] = 0.0;
  _mat[ 1 ][ 3 ] = 0.0;
  _mat[ 2 ][ 3 ] = 0.0;
  _mat[ 3 ][ 3 ] = 1.0;
}

Matrix::Matrix( const Vector& x, const Vector& y, const Vector& z, const Point& o )
{
  _mat[ 0 ][ 0 ] = x[ 0 ];
  _mat[ 1 ][ 0 ] = x[ 1 ];
  _mat[ 2 ][ 0 ] = x[ 2 ];
  _mat[ 3 ][ 0 ] = x[ 3 ];

  _mat[ 0 ][ 1 ] = y[ 0 ];
  _mat[ 1 ][ 1 ] = y[ 1 ];
  _mat[ 2 ][ 1 ] = y[ 2 ];
  _mat[ 3 ][ 1 ] = y[ 3 ];

  _mat[ 0 ][ 2 ] = z[ 0 ];
  _mat[ 1 ][ 2 ] = z[ 1 ];
  _mat[ 2 ][ 2 ] = z[ 2 ];
  _mat[ 3 ][ 2 ] = z[ 3 ];

  _mat[ 0 ][ 3 ] = o[ 0 ];
  _mat[ 1 ][ 3 ] = o[ 1 ];
  _mat[ 2 ][ 3 ] = o[ 2 ];
  _mat[ 3 ][ 3 ] = o[ 3 ];
}

Matrix::Matrix( const Matrix& rhs )
{
  _mat[ 0 ][ 0 ] = rhs[ 0 ][ 0 ];
  _mat[ 1 ][ 0 ] = rhs[ 1 ][ 0 ];
  _mat[ 2 ][ 0 ] = rhs[ 2 ][ 0 ];
  _mat[ 3 ][ 0 ] = rhs[ 3 ][ 0 ];

  _mat[ 0 ][ 1 ] = rhs[ 0 ][ 1 ];
  _mat[ 1 ][ 1 ] = rhs[ 1 ][ 1 ];
  _mat[ 2 ][ 1 ] = rhs[ 2 ][ 1 ];
  _mat[ 3 ][ 1 ] = rhs[ 3 ][ 1 ];

  _mat[ 0 ][ 2 ] = rhs[ 0 ][ 2 ];
  _mat[ 1 ][ 2 ] = rhs[ 1 ][ 2 ];
  _mat[ 2 ][ 2 ] = rhs[ 2 ][ 2 ];
  _mat[ 3 ][ 2 ] = rhs[ 3 ][ 2 ];

  _mat[ 0 ][ 3 ] = rhs[ 0 ][ 3 ];
  _mat[ 1 ][ 3 ] = rhs[ 1 ][ 3 ];
  _mat[ 2 ][ 3 ] = rhs[ 2 ][ 3 ];
  _mat[ 3 ][ 3 ] = rhs[ 3 ][ 3 ];
}

Matrix::Matrix( Matrix&& rhs )
{
  _mat[ 0 ][ 0 ] = rhs[ 0 ][ 0 ];
  _mat[ 1 ][ 0 ] = rhs[ 1 ][ 0 ];
  _mat[ 2 ][ 0 ] = rhs[ 2 ][ 0 ];
  _mat[ 3 ][ 0 ] = rhs[ 3 ][ 0 ];

  _mat[ 0 ][ 1 ] = rhs[ 0 ][ 1 ];
  _mat[ 1 ][ 1 ] = rhs[ 1 ][ 1 ];
  _mat[ 2 ][ 1 ] = rhs[ 2 ][ 1 ];
  _mat[ 3 ][ 1 ] = rhs[ 3 ][ 1 ];

  _mat[ 0 ][ 2 ] = rhs[ 0 ][ 2 ];
  _mat[ 1 ][ 2 ] = rhs[ 1 ][ 2 ];
  _mat[ 2 ][ 2 ] = rhs[ 2 ][ 2 ];
  _mat[ 3 ][ 2 ] = rhs[ 3 ][ 2 ];

  _mat[ 0 ][ 3 ] = rhs[ 0 ][ 3 ];
  _mat[ 1 ][ 3 ] = rhs[ 1 ][ 3 ];
  _mat[ 2 ][ 3 ] = rhs[ 2 ][ 3 ];
  _mat[ 3 ][ 3 ] = rhs[ 3 ][ 3 ];
}

Matrix::~Matrix()
{
}

Matrix& Matrix::operator= ( const Matrix& rhs )
{
  _mat[ 0 ][ 0 ] = rhs[ 0 ][ 0 ];
  _mat[ 1 ][ 0 ] = rhs[ 1 ][ 0 ];
  _mat[ 2 ][ 0 ] = rhs[ 2 ][ 0 ];
  _mat[ 3 ][ 0 ] = rhs[ 3 ][ 0 ];

  _mat[ 0 ][ 1 ] = rhs[ 0 ][ 1 ];
  _mat[ 1 ][ 1 ] = rhs[ 1 ][ 1 ];
  _mat[ 2 ][ 1 ] = rhs[ 2 ][ 1 ];
  _mat[ 3 ][ 1 ] = rhs[ 3 ][ 1 ];

  _mat[ 0 ][ 2 ] = rhs[ 0 ][ 2 ];
  _mat[ 1 ][ 2 ] = rhs[ 1 ][ 2 ];
  _mat[ 2 ][ 2 ] = rhs[ 2 ][ 2 ];
  _mat[ 3 ][ 2 ] = rhs[ 3 ][ 2 ];

  _mat[ 0 ][ 3 ] = rhs[ 0 ][ 3 ];
  _mat[ 1 ][ 3 ] = rhs[ 1 ][ 3 ];
  _mat[ 2 ][ 3 ] = rhs[ 2 ][ 3 ];
  _mat[ 3 ][ 3 ] = rhs[ 3 ][ 3 ];
  return *this;
}

Matrix& Matrix::operator= ( Matrix&& rhs )
{
  _mat[ 0 ][ 0 ] = rhs[ 0 ][ 0 ];
  _mat[ 1 ][ 0 ] = rhs[ 1 ][ 0 ];
  _mat[ 2 ][ 0 ] = rhs[ 2 ][ 0 ];
  _mat[ 3 ][ 0 ] = rhs[ 3 ][ 0 ];

  _mat[ 0 ][ 1 ] = rhs[ 0 ][ 1 ];
  _mat[ 1 ][ 1 ] = rhs[ 1 ][ 1 ];
  _mat[ 2 ][ 1 ] = rhs[ 2 ][ 1 ];
  _mat[ 3 ][ 1 ] = rhs[ 3 ][ 1 ];

  _mat[ 0 ][ 2 ] = rhs[ 0 ][ 2 ];
  _mat[ 1 ][ 2 ] = rhs[ 1 ][ 2 ];
  _mat[ 2 ][ 2 ] = rhs[ 2 ][ 2 ];
  _mat[ 3 ][ 2 ] = rhs[ 3 ][ 2 ];

  _mat[ 0 ][ 3 ] = rhs[ 0 ][ 3 ];
  _mat[ 1 ][ 3 ] = rhs[ 1 ][ 3 ];
  _mat[ 2 ][ 3 ] = rhs[ 2 ][ 3 ];
  _mat[ 3 ][ 3 ] = rhs[ 3 ][ 3 ];
  return *this;
}

double* Matrix::operator[] ( int row )
{
  return _mat[ row ];
}

const double* Matrix::operator[] ( int row ) const
{
  return _mat[ row ];
}

Matrix& Matrix::operator*= ( const Matrix& rhs )
{
  Matrix m = *this;

  _mat[ 0 ][ 0 ] = m[ 0 ][ 0 ] * rhs[ 0 ][ 0 ] + m[ 0 ][ 1 ] * rhs[ 1 ][ 0 ] + m[ 0 ][ 2 ] * rhs[ 2 ][ 0 ] + m[ 0 ][ 3 ] * rhs[ 3 ][ 0 ];
  _mat[ 0 ][ 1 ] = m[ 0 ][ 0 ] * rhs[ 0 ][ 1 ] + m[ 0 ][ 1 ] * rhs[ 1 ][ 1 ] + m[ 0 ][ 2 ] * rhs[ 2 ][ 1 ] + m[ 0 ][ 3 ] * rhs[ 3 ][ 1 ];
  _mat[ 0 ][ 2 ] = m[ 0 ][ 0 ] * rhs[ 0 ][ 2 ] + m[ 0 ][ 1 ] * rhs[ 1 ][ 2 ] + m[ 0 ][ 2 ] * rhs[ 2 ][ 2 ] + m[ 0 ][ 3 ] * rhs[ 3 ][ 2 ];
  _mat[ 0 ][ 3 ] = m[ 0 ][ 0 ] * rhs[ 0 ][ 3 ] + m[ 0 ][ 1 ] * rhs[ 1 ][ 3 ] + m[ 0 ][ 2 ] * rhs[ 2 ][ 3 ] + m[ 0 ][ 3 ] * rhs[ 3 ][ 3 ];

  _mat[ 1 ][ 0 ] = m[ 1 ][ 0 ] * rhs[ 0 ][ 0 ] + m[ 1 ][ 1 ] * rhs[ 1 ][ 0 ] + m[ 1 ][ 2 ] * rhs[ 2 ][ 0 ] + m[ 1 ][ 3 ] * rhs[ 3 ][ 0 ];
  _mat[ 1 ][ 1 ] = m[ 1 ][ 0 ] * rhs[ 0 ][ 1 ] + m[ 1 ][ 1 ] * rhs[ 1 ][ 1 ] + m[ 1 ][ 2 ] * rhs[ 2 ][ 1 ] + m[ 1 ][ 3 ] * rhs[ 3 ][ 1 ];
  _mat[ 1 ][ 2 ] = m[ 1 ][ 0 ] * rhs[ 0 ][ 2 ] + m[ 1 ][ 1 ] * rhs[ 1 ][ 2 ] + m[ 1 ][ 2 ] * rhs[ 2 ][ 2 ] + m[ 1 ][ 3 ] * rhs[ 3 ][ 2 ];
  _mat[ 1 ][ 3 ] = m[ 1 ][ 0 ] * rhs[ 0 ][ 3 ] + m[ 1 ][ 1 ] * rhs[ 1 ][ 3 ] + m[ 1 ][ 2 ] * rhs[ 2 ][ 3 ] + m[ 1 ][ 3 ] * rhs[ 3 ][ 3 ];

  _mat[ 2 ][ 0 ] = m[ 2 ][ 0 ] * rhs[ 0 ][ 0 ] + m[ 2 ][ 1 ] * rhs[ 1 ][ 0 ] + m[ 2 ][ 2 ] * rhs[ 2 ][ 0 ] + m[ 2 ][ 3 ] * rhs[ 3 ][ 0 ];
  _mat[ 2 ][ 1 ] = m[ 2 ][ 0 ] * rhs[ 0 ][ 1 ] + m[ 2 ][ 1 ] * rhs[ 1 ][ 1 ] + m[ 2 ][ 2 ] * rhs[ 2 ][ 1 ] + m[ 2 ][ 3 ] * rhs[ 3 ][ 1 ];
  _mat[ 2 ][ 2 ] = m[ 2 ][ 0 ] * rhs[ 0 ][ 2 ] + m[ 2 ][ 1 ] * rhs[ 1 ][ 2 ] + m[ 2 ][ 2 ] * rhs[ 2 ][ 2 ] + m[ 2 ][ 3 ] * rhs[ 3 ][ 2 ];
  _mat[ 2 ][ 3 ] = m[ 2 ][ 0 ] * rhs[ 0 ][ 3 ] + m[ 2 ][ 1 ] * rhs[ 1 ][ 3 ] + m[ 2 ][ 2 ] * rhs[ 2 ][ 3 ] + m[ 2 ][ 3 ] * rhs[ 3 ][ 3 ];

  _mat[ 3 ][ 0 ] = m[ 3 ][ 0 ] * rhs[ 0 ][ 0 ] + m[ 3 ][ 1 ] * rhs[ 1 ][ 0 ] + m[ 3 ][ 2 ] * rhs[ 2 ][ 0 ] + m[ 3 ][ 3 ] * rhs[ 3 ][ 0 ];
  _mat[ 3 ][ 1 ] = m[ 3 ][ 0 ] * rhs[ 0 ][ 1 ] + m[ 3 ][ 1 ] * rhs[ 1 ][ 1 ] + m[ 3 ][ 2 ] * rhs[ 2 ][ 1 ] + m[ 3 ][ 3 ] * rhs[ 3 ][ 1 ];
  _mat[ 3 ][ 2 ] = m[ 3 ][ 0 ] * rhs[ 0 ][ 2 ] + m[ 3 ][ 1 ] * rhs[ 1 ][ 2 ] + m[ 3 ][ 2 ] * rhs[ 2 ][ 2 ] + m[ 3 ][ 3 ] * rhs[ 3 ][ 2 ];
  _mat[ 3 ][ 3 ] = m[ 3 ][ 0 ] * rhs[ 0 ][ 3 ] + m[ 3 ][ 1 ] * rhs[ 1 ][ 3 ] + m[ 3 ][ 2 ] * rhs[ 2 ][ 3 ] + m[ 3 ][ 3 ] * rhs[ 3 ][ 3 ];

  return *this;
}

Matrix& Matrix::Inverse()
{
  return *this;
}

Matrix& Matrix::Transpose()
{
  Matrix temp = *this;

  _mat[ 0 ][ 0 ] = temp[ 0 ][ 0 ];
  _mat[ 1 ][ 0 ] = temp[ 0 ][ 1 ];
  _mat[ 2 ][ 0 ] = temp[ 0 ][ 2 ];
  _mat[ 3 ][ 0 ] = temp[ 0 ][ 3 ];

  _mat[ 0 ][ 1 ] = temp[ 1 ][ 0 ];
  _mat[ 1 ][ 1 ] = temp[ 1 ][ 1 ];
  _mat[ 2 ][ 1 ] = temp[ 1 ][ 2 ];
  _mat[ 3 ][ 1 ] = temp[ 1 ][ 3 ];

  _mat[ 0 ][ 2 ] = temp[ 2 ][ 0 ];
  _mat[ 1 ][ 2 ] = temp[ 2 ][ 1 ];
  _mat[ 2 ][ 2 ] = temp[ 2 ][ 2 ];
  _mat[ 3 ][ 2 ] = temp[ 2 ][ 3 ];

  _mat[ 0 ][ 3 ] = temp[ 3 ][ 0 ];
  _mat[ 1 ][ 3 ] = temp[ 3 ][ 1 ];
  _mat[ 2 ][ 3 ] = temp[ 3 ][ 2 ];
  _mat[ 3 ][ 3 ] = temp[ 3 ][ 3 ];

  return *this;
}

Matrix Translate( double x, double y, double z )
{
  Matrix m;
  m[ 0 ][ 3 ] = x;
  m[ 1 ][ 3 ] = y;
  m[ 2 ][ 3 ] = z;
  return m;
}

Matrix Scale( double x, double y, double z )
{
  Matrix m;
  m[ 0 ][ 0 ] = x;
  m[ 1 ][ 1 ] = y;
  m[ 2 ][ 2 ] = z;
  return m;
}

Matrix Matrix::Rotate( const Axis& axis, double radians )
{
  Matrix m;
  switch( axis )
  {
    case Axis::X:
      m = RotateX( radians );
      break;
    case Axis::Y:
      m = RotateY( radians );
      break;
    case Axis::Z:
      m = RotateZ( radians );
      break;
  }
  return m;
}

Matrix Matrix::RotateX( double radians )
{
  Matrix m;
  double cosTheta = cos( radians );
  double sinTheta = sin( radians );

  m[ 1 ][ 1 ] = cosTheta;
  m[ 1 ][ 2 ] = -sinTheta;
  m[ 2 ][ 1 ] = sinTheta;
  m[ 2 ][ 2 ] = cosTheta;

  return m;
}

Matrix Matrix::RotateY( double radians )
{
  Matrix m;
  double cosTheta = cos( radians );
  double sinTheta = sin( radians );

  m[ 0 ][ 0 ] = cosTheta;
  m[ 0 ][ 2 ] = -sinTheta;
  m[ 2 ][ 0 ] = sinTheta;
  m[ 2 ][ 2 ] = cosTheta;

  return m;
}

Matrix Matrix::RotateZ( double radians )
{
  Matrix m;
  double cosTheta = cos( radians );
  double sinTheta = sin( radians );

  m[ 0 ][ 0 ] = cosTheta;
  m[ 0 ][ 1 ] = -sinTheta;
  m[ 1 ][ 0 ] = sinTheta;
  m[ 1 ][ 1 ] = cosTheta;

  return m;
}

Matrix pdrt::operator* ( const Matrix& left, const Matrix& right )
{
  Matrix m = left;
  m *= right;
  return m;
}

Point pdrt::operator* ( const Matrix& left, const Point& right )
{
  Point p;
  p[ 0 ] = left[ 0 ][ 0 ] * right[ 0 ] + left[ 0 ][ 1 ] * right[ 1 ] + left[ 0 ][ 2 ] * right[ 2 ] + left[ 0 ][ 3 ] * right[ 3 ];
  p[ 1 ] = left[ 1 ][ 0 ] * right[ 0 ] + left[ 1 ][ 1 ] * right[ 1 ] + left[ 1 ][ 2 ] * right[ 2 ] + left[ 1 ][ 3 ] * right[ 3 ];
  p[ 2 ] = left[ 2 ][ 0 ] * right[ 0 ] + left[ 2 ][ 1 ] * right[ 1 ] + left[ 2 ][ 2 ] * right[ 2 ] + left[ 2 ][ 3 ] * right[ 3 ];
  p[ 3 ] = left[ 3 ][ 0 ] * right[ 0 ] + left[ 3 ][ 1 ] * right[ 1 ] + left[ 3 ][ 2 ] * right[ 2 ] + left[ 3 ][ 3 ] * right[ 3 ];
  return p;
}

Vector pdrt::operator* ( const Matrix& left, const Vector& right )
{
  Vector v;
  v[ 0 ] = left[ 0 ][ 0 ] * right[ 0 ] + left[ 0 ][ 1 ] * right[ 1 ] + left[ 0 ][ 2 ] * right[ 2 ] + left[ 0 ][ 3 ] * right[ 3 ];
  v[ 1 ] = left[ 1 ][ 0 ] * right[ 0 ] + left[ 1 ][ 1 ] * right[ 1 ] + left[ 1 ][ 2 ] * right[ 2 ] + left[ 1 ][ 3 ] * right[ 3 ];
  v[ 2 ] = left[ 2 ][ 0 ] * right[ 0 ] + left[ 2 ][ 1 ] * right[ 1 ] + left[ 2 ][ 2 ] * right[ 2 ] + left[ 2 ][ 3 ] * right[ 3 ];
  v[ 3 ] = left[ 3 ][ 0 ] * right[ 0 ] + left[ 3 ][ 1 ] * right[ 1 ] + left[ 3 ][ 2 ] * right[ 2 ] + left[ 3 ][ 3 ] * right[ 3 ];
  return v;
}

Normal pdrt::operator* ( const Matrix& left, const Normal& right )
{
  Normal n;
  n[ 0 ] = left[ 0 ][ 0 ] * right[ 0 ] + left[ 0 ][ 1 ] * right[ 1 ] + left[ 0 ][ 2 ] * right[ 2 ] + left[ 0 ][ 3 ] * right[ 3 ];
  n[ 1 ] = left[ 1 ][ 0 ] * right[ 0 ] + left[ 1 ][ 1 ] * right[ 1 ] + left[ 1 ][ 2 ] * right[ 2 ] + left[ 1 ][ 3 ] * right[ 3 ];
  n[ 2 ] = left[ 2 ][ 0 ] * right[ 0 ] + left[ 2 ][ 1 ] * right[ 1 ] + left[ 2 ][ 2 ] * right[ 2 ] + left[ 2 ][ 3 ] * right[ 3 ];
  n[ 3 ] = left[ 3 ][ 0 ] * right[ 0 ] + left[ 3 ][ 1 ] * right[ 1 ] + left[ 3 ][ 2 ] * right[ 2 ] + left[ 3 ][ 3 ] * right[ 3 ];
  return n;
}

Ray pdrt::operator* ( const Matrix& left, const Ray& right )
{
  Ray r;
  // TODO: figure out M * r
  return r;
}

Matrix pdrt::Inverse( const Matrix& m )
{
  Matrix ret = m;
  ret.Inverse();
  return ret;
}

Matrix pdrt::Transpose( const Matrix& m )
{
  Matrix ret = m;
  ret.Transpose();
  return ret;
}